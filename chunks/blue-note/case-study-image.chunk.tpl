    <div class="slideshow-item text-hide">
      <a href="[[~[[+id]]]]">
        <img src="[[+tv.pageImage]]" />
        <div class="slideshow-title">
          [[+pagetitle]]<br /><span class="accent-font">[[+longtitle]]</span>
        </div>
      </a>
    </div>
